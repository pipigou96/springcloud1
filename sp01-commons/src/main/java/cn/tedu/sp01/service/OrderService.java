package cn.tedu.sp01.service;

import cn.tedu.sp01.pojo.Order;

/**
 * 作者：小郭
 * 创建时间：2020/11/2311:19
 */
public interface OrderService {
    Order getOrder(String orderId);
    void addOrder(Order order);
}
