package cn.tedu.sp01.service;

import cn.tedu.sp01.pojo.User;

/**
 * 作者：小郭
 * 创建时间：2020/11/2311:20
 */
public interface UserService {
    User getUser(Integer id);
    void addScore(Integer id, Integer score);
}
