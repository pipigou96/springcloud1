package cn.tedu.sp06;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
//@EnableCircuitBreaker 自动熔断
//@EnableDiscoveryClient
//@SpringBootApplication

@SpringCloudApplication
public class Sp07RibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sp07RibbonApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        /*设置ribbo的超时时间，超时后就默认调用失败*/
        SimpleClientHttpRequestFactory f=new SimpleClientHttpRequestFactory();
        f.setConnectTimeout(1000);//建立连接等待时间
        f.setReadTimeout(1000); //建立连接后，发送请求后，等待接收响应的时间
        return new RestTemplate(f);
        //RestTemplate中默认的Factory实例中，两个超时属性默认是-1
        //未启用超时，也不会触发重试
        //return new RestTemplate();
    }
}



